define(function() {
  "use strict";
  
  // Function for data validation
  function checking(users, username, n1, n2, email, p1, p2, age) {
    // Check if the username is already exist
    if (users.length > 0) {
      for (var user of users) {
        if (user.username === username) {
          alert("Username has already be used.");
          return false;
        }
        if (user.email === email) {
          alert("Email has already be used.");
          return false;
        }
      }
    }

    // Validate first name and last name
    if ((/^[A-Za-z\s]+$/.test(n1)) === false || n1.length < 3) {
      alert("Invalid first name. (Alphabet only)");
      return false;
    }
    if ((/^[A-Za-z\s]+$/.test(n2)) === false || n2.length < 2) {
      alert("Invalid last name. (Alphabet only)");
      return false;
    }

    // Validate the email address
    if (!email || (email.length < 6) || (email.indexOf('@') == -1)) {
      alert('Please enter a valid email address!');
      return false;
    }

    // Validate password
    if (p1.length < 6 || p2.length < 6) {
      alert('The length of password must >= 6!');
      return false;
    }

    // Compare two entered password
    if (p1 != p2) {
      alert('Two password are not the same!');
      return false;
    }

    // Validate the age
    if (age <= 0 || age > 150) {
      alert('Age must between 1 and 150');
      return false;
    }
  }

  return {
    "checking": checking
  };
});
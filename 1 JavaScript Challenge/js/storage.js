define(function() {
  "use strict";

  var url = "https://js-assignment1-d2ed2.firebaseio.com/users.json";
  
  // Function to create AJAX object
  function setupAJAX() {
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
      var request = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE 8 and older
      var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return request;
  }
  
  // Function to get all accounts from Firebase
  function getAllUsers() {
    // Return a new promuse object
    return new Promise(function(resolve, reject) {
      var request = setupAJAX();
      request.open("GET", url);

      request.onload = () => {
        if (request.status === 200) {
          resolve(request.response);
        } else {
          reject(Error(request.statusText));
        }
      };

      request.onerror = () => {
        reject(Error("Network Error"));
      };

      request.send();
    });
  }
  
  // Function to add a new user to Firebase database
  function addUser(users) {
    var request = setupAJAX();
    // Return a new promuse object
    return new Promise(function(resolve, reject) {
      request.open("POST", url);

      request.onload = () => {
        if (request.status === 200) {
          resolve(request.response);
          alert("Register successfully!");
        } else {
          reject(alert(Error(request.statusText)));
        }
      };

      request.onerror = () => {
        reject(alert(Error("Network Error")));
      };

      request.send(JSON.stringify(users));
    });
  }

  return {
    "getAllUsers": getAllUsers,
    "addUser": addUser
  };
});
define(function() {
  "use strict";
  
  // Control the timing of showing the table that contain currently registered accounts
  function setStyle(users) {
    if (users.length === 0) {
      document.getElementById('showUsers').style.display = 'none';
    } else {
      document.getElementById('showUsers').style.display = 'block';
      showList(users);
    }
  }
  
  // Function to generate rows of table
  function showList(users) {
    var columns = ["username", "firstname", "lastname", "sex", "email", "age"];
    var user_list = document.getElementById('user_list');

    // First clean up the current user list(table)
    for (var i = user_list.rows.length - 1; i > 0; i--) {
      user_list.deleteRow(i);
    }

    // Create new user list(table)
    users.forEach(function(user) {
      var row = document.createElement('tr');
      user_list.appendChild(row);

      for (var a = 0; a < columns.length; a++) {
        var column = document.createElement('td');
        column.innerHTML = user[columns[a]];
        row.appendChild(column);
      }
    });
  }

  return {
    "setStyle": setStyle,
    "showList": showList
  };
});
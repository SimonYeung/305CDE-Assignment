require(['checking', 'table', 'storage'], function(checking, table, firebase) {
  "use strict";

  var user_list = [];
  
  // When the app started, get all existing account from Firebase database
  // Then update the page to show a table if there are currently registered accounts
  firebase.getAllUsers()
    .then(JSON.parse)
    .then(function(listFromFirebase) {
      if (listFromFirebase != null) {
        for (var index in listFromFirebase) {
          user_list.push(listFromFirebase[index]);
        }
      }
    })
    .then(function() {
      table.setStyle(user_list);
    });

  // Onclick listener of the form submit button
  document.getElementById('submit').onclick = function() {
    var fields = document.querySelectorAll('#theForm input');

    // Check if there any field is empty
    for (var field of fields) {
      if (field.value.length === 0) {
        alert("Please fill in all the fields!");
        return false;
      }
    }
  
    // Get values from each fields
    var username = document.getElementById('username').value;
    var firstname = document.getElementById('firstname').value;
    var lastname = document.getElementById('lastname').value;
    var sex = document.getElementById('sex').value;
    var email = document.getElementById('email').value;
    var p1 = document.getElementById('password').value;
    var p2 = document.getElementById('confirm').value;
    var age = document.getElementById('age').value;

    // Call checking function of checking module
    // If the checking passed, create a new user object and push it to the users array
    var checkingResult = checking.checking(user_list, username, firstname, lastname, email, p1, p2, age);
    if (checkingResult !== false) {
      var user = {
        "username": username,
        "firstname": firstname,
        "lastname": lastname,
        "sex": sex,
        "email": email,
        "password": p1,
        "age": age
      };
      firebase.addUser(user).then(function() {
        user_list.push(user);
        table.setStyle(user_list);
      }).catch(function(error) {
        alert(error.message);
      }).then(function() {
        document.getElementById('theForm').reset();
      });
    }
    
    // Return false in the end of onclick function to prevent the app reload the page 
    return false;
  };
});